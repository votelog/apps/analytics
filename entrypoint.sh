#!/bin/sh

# download GeoIP DB the first time
geoipupdate

# create crond script to weekly update GeoIP DB
printf '#!/bin/sh\n\ngeoipupdate\n' > /etc/periodic/weekly/geoipupdate.sh
chmod +x /etc/periodic/weekly/geoipupdate.sh

# start GoatCounter
goatcounter serve -listen :8080 -tls proxy -automigrate -geodb /usr/local/share/GeoIP/GeoLite2-City.mmdb
