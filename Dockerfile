# syntax=docker/dockerfile:1

# use stable Alpine Linux as builder
FROM golang:alpine AS builder

## install GoatCounter build deps
RUN apk add --no-cache \
  build-base \
  git

## clone GoatCounter repo and build
RUN git clone --branch=master --depth 1 https://github.com/arp242/goatcounter.git && \
  cd goatcounter && \
  go build \
    -o / \
    -tags osusergo,netgo,sqlite_omit_load_extension \
    -ldflags="-X zgo.at/goatcounter/v2.Version=$(git log -n1 --format='%h_%cI') -extldflags=-static" \
    ./cmd/goatcounter

# use latest Alpine Linux as runner
FROM alpine:latest

## install run deps
## - `busybox-openrc` is required for crond
## - `go` and `graphviz` are required for internal performance metrics reporting
## - `sqlite` is required to manually inspect the DB
RUN apk add --no-cache \
  busybox-openrc \
  go \
  graphviz \
  sqlite

## copy necessary files
COPY --link --from=builder /goatcounter /usr/local/bin/goatcounter
COPY --link --from=ghcr.io/maxmind/geoipupdate:latest /usr/bin/geoipupdate /usr/local/bin/geoipupdate
COPY --link entrypoint.sh /entrypoint.sh

## ignored by Fly
VOLUME [ "/db" ]

## expose network port and set entry point
EXPOSE 8080
ENTRYPOINT ["/entrypoint.sh"]
