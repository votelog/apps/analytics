# VoteLog analytics

[![Health](https://status.votelog.ch/api/v1/endpoints/internal-tools_analytics-(goatcounter)/health/badge.svg)](https://status.votelog.ch/endpoints/internal-tools_analytics-(goatcounter))
[![Uptime (30 days)](https://status.votelog.ch/api/v1/endpoints/internal-tools_analytics-(goatcounter)/uptimes/30d/badge.svg)](https://status.votelog.ch/endpoints/internal-tools_analytics-(goatcounter))
[![Response time (30 days)](https://status.votelog.ch/api/v1/endpoints/internal-tools_analytics-(goatcounter)/response-times/30d/badge.svg)](https://status.votelog.ch/endpoints/internal-tools_analytics-(goatcounter))

This repository contains non-sensitive code and configuration related to the VoteLog analytics tool available under
[**`analytics.votelog.ch`**](https://analytics.votelog.ch/).

We rely on [**GoatCounter**](https://www.goatcounter.com/) which we host on [Fly](https://fly.io/).

## Configuration

GoatCounter can be conveniently configued via its web interface. The configuration values are stored in its SQLite database which lives on a persistent storage
volume (see [hosting details](#hosting-details) below) and thus survive re-deploys.

Note that

-   we configure [Fly Proxy](https://fly.io/docs/reference/fly-proxy/) to forward *HTTP/2 cleartext (H2C) with prior knowledge* by setting
    [`services.ports.http_options.h2_backend = true`](https://fly.io/docs/reference/configuration/#services-ports-http_options-h2_backend), so that HTTP/2
    connections aren't unnecessarily downgraded to HTTP/1 in between (which harms performance). We still configure Fly Proxy to add some HTTP headers that
    GoatCounter doesn't set itself. See GoatCounter's source code for the headers that are set by default:
    -   [`mw.go`](https://github.com/arp242/goatcounter/blob/master/handlers/mw.go#L368-L383) for CSP.
    -   [`handlers/backend.go`](https://github.com/arp242/goatcounter/blob/master/handlers/backend.go#L119-L123) for other headers.
-   we don't need to run [`setcap 'cap_net_bind_service=+ep'`](https://stackoverflow.com/a/414258/7196903) as [recommended in the
    doc](https://github.com/arp242/goatcounter/tree/master#running) since we don't bind to a port below 1024 (but 8080 instead which is mapped to 80/443 by
    Fly Proxy).

Sensitive configuration data is stored via [Fly secrets](https://fly.io/docs/reference/secrets/). Currently this includes the following environment variables:

-   `GEOIPUPDATE_ACCOUNT_ID`
-   `GEOIPUPDATE_LICENSE_KEY`

## Hosting details {#hosting-details}

The Fly [app](https://fly.io/docs/reference/apps/) is named `votelog-analytics` and the [persistent storage volume](https://fly.io/docs/reference/volumes/) is
named `votelog_analytics` with a current size of 1 GiB[^readme-1]. Its unique ID is `vol_6r77oe3m50x3p3yr`.

Currently, we use an [SQLite](https://en.wikipedia.org/wiki/SQLite) database and run only a single [`shared-cpu-1x` instance with `256 MB`
RAM](https://fly.io/docs/about/pricing/#compute) hosted in the [*Paris, France* region](https://fly.io/docs/reference/regions/). Should it prove necessary to
expand performance for our main users (located in Switzerland), we could [increase RAM](https://fly.io/docs/flyctl/scale-memory/) and/or [switch to a faster
CPU](https://fly.io/docs/flyctl/scale-vm/) anytime[^readme-2].

Should we also want to provide fast access for non-European users, we could [run multiple instances](https://fly.io/docs/apps/scale-count/) of GoatCounter in
multiple regions. This would either require to replicate the SQLite database using [LiteFS](https://fly.io/docs/litefs/), or to switch to
[PostgreSQL](https://fly.io/docs/getting-started/multi-region-databases/). However, this most probably won't be necessary in the forseeable future.

[^readme-1]: The volume size can always be [extended](https://fly.io/docs/flyctl/volumes-extend/). To extend it to 5 GiB for example, simply run:

    ``` sh
    flyctl volumes extend vol_6r77oe3m50x3p3yr --size=5
    ```

    Note that this will restart the app.

[^readme-2]: Note that scaling automatically restarts the app. The most relevant documentation on (auto)scaling Fly apps includes:

    -   [Scale Machine CPU and RAM](https://fly.io/docs/apps/scale-machine/)
    -   [Scale the Number of Machines](https://fly.io/docs/apps/scale-count/)
    -   [Automatically Stop and Start Machines](https://fly.io/docs/apps/autostart-stop/)

## Admin access

We can directly connect into the root filesystem of the Fly app via an [SSH tunnel](https://fly.io/docs/flyctl/ssh-console/) to perform any low-level
administration tasks or directly access the database. The SQLite database file is stored under `/db/goatcounter.sqlite3`.

To directly log into the database using the [SQLite CLI](https://www.sqlite.org/cli.html) over SSH, run:

``` sh
flyctl ssh console -C 'sqlite3 /db/goatcounter.sqlite3'
```

To exit the SQLite CLI, press <kbd>Ctrl</kbd>+<kbd>D</kbd>, and then enter `exit` to terminate the SSH tunnel.

We can also fetch a copy of the SQLite database to inspect it locally[^readme-3] using SFTP:

``` sh
flyctl sftp get /db/goatcounter.sqlite3 /PATH/TO/LOCAL_FILE.db
```

[^readme-3]: E.g. using [DB Browser for SQLite](https://sqlitebrowser.org/).

## License

Code and configuration in this repository are licensed under [`AGPL-3.0-or-later`](https://spdx.org/licenses/AGPL-3.0-or-later.html). See
[LICENSE.md](LICENSE.md).

----------------------------------------------------------------------------------------------------------------------------------------------------------------
