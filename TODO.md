# TODOs

- figure out why GoatCounter doesn't count languages. likely cause: the `Accept-Language` HTTP header is not forwarded by Fly Proxy. see [this comment](https://github.com/arp242/goatcounter/issues/294#issuecomment-642653514) on how to debug IP address (could be similar for language header)

- configure error reporting via E-Mail, see `goatcounter serve --help`
  - sender via `-email-from`
  - SMTP relay server via `-smtp`
